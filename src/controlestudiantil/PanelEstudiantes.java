/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;


import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;


/**
 *
 * @author David
 */
public final class PanelEstudiantes extends javax.swing.JPanel {

    /**
     * Creates new form PanelEstudiantes
     */
    ArrayList<Estudiante> lista_de_estudiantes;
    Conector conector_base;
    FormularioEstudiante formulario_estudiante;
    
    public PanelEstudiantes() {
        initComponents();
        conector_base = new Conector();        
       
        //LimpiarTabla();
        ObtenerEstudiantes();
        
    }
    public void NuevoEstudiante()
    {
        this.formulario_estudiante = null;
        formulario_estudiante = new FormularioEstudiante(this);
        formulario_estudiante.setEstudiante(new Estudiante());
        formulario_estudiante.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                //formulario_estudiante.setVisible(false);
                //actualizar lista de estudiantes
                ObtenerEstudiantes();
                formulario_estudiante.setVisible(false);
                /*
                if (JOptionPane.showConfirmDialog(formulario_estudiante, 
                    "Are you sure you want to close this window?", "Close Window?", 
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
                    System.exit(0);
                }*/
            }

           
            
        });
        formulario_estudiante.setVisible(true);
    }
    public void EditarEstudiante(int index)
    {
        Estudiante estudiante =(Estudiante) lista_de_estudiantes.get(index);
        this.formulario_estudiante = null;
        this.formulario_estudiante = new FormularioEstudiante(this);
        this.formulario_estudiante.setEstudiante(estudiante);
        this.formulario_estudiante.setVisible(true);
    }
    public void ObtenerEstudiantes()
    {
        
        this.lista_de_estudiantes = conector_base.ListaDeEstudiantes();
       
        String col[] = {"Nro","Nombres","Apellidos", "Telefono","Nivel","Genero"};

        DefaultTableModel model = new DefaultTableModel(col, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
               //all cells false
               return false;
            }
        };
        jTable1.setModel(model);
        
       
       // DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        lista_de_estudiantes.forEach((estudiante) -> {
            model.addRow(new Object[]{estudiante.getId(), estudiante.getNombres(),estudiante.getApellidos(),estudiante.getTelefono(),estudiante.getNivel(),estudiante.getGenero()});
        });
        
        /*for(int i=0;i<this.lista_de_estudiantes.size();i++){
            Estudiante estudiante = (Estudiante) this.lista_de_estudiantes.get(i);
             model.addRow(new Object[]{estudiante.getId(), estudiante.getNombres(),estudiante.getApellidos(),estudiante.getTelefono(),estudiante.getNivel(),estudiante.getGenero()});
        }*/
        
        jTable1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int index_estudiante = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
                    // your valueChanged overridden method 
                     mouseEvent.consume();
                    System.out.println("Fila : "+index_estudiante);
                    EditarEstudiante(index_estudiante);
                    
                }
            }
        });
        
    
        jTable1.validate();
        jTable1.repaint();
       
    }
    public void LimpiarTabla()
    {
         String col[] = {"Nro","Nombres","Apellidos", "Edad"};

        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        jTable1.setModel(tableModel);
     
    }
    public void RegistrarEstudiante(Estudiante estudiante)
    {
        System.out.println("Registrando "+estudiante.getNombres());
        conector_base.GuardarEstudiante(estudiante);
        this.formulario_estudiante.dispose();
        this.formulario_estudiante =null;
        ObtenerEstudiantes();
    }
    
    public void BorrarEstudiante(Estudiante estudiante)
    {
        System.out.println("Eliminando "+estudiante.getNombres());
        conector_base.EliminarEstudiante(estudiante);
        this.formulario_estudiante.dispose();
        this.formulario_estudiante = null;
        ObtenerEstudiantes();
    }
            
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Estudiantes");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/controlestudiantil/images/refresh_black.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/controlestudiantil/images/person_black.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3)
                    .addComponent(jButton2)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        ObtenerEstudiantes();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        NuevoEstudiante();
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
/*
    class ButtonTableCellRenderer implements TableCellRenderer{
 
         
           JTable table;
           JButton renderButton;
           JButton editButton;
           String text;
     
           public ButtonTableCellRenderer(JTable table, int column)
           {
               super();
               this.table = table;
               renderButton = new JButton("Editar");
     
               editButton = new JButton();
              // editButton.setFocusPainted( false );
         
     
               TableColumnModel columnModel = table.getColumnModel();
               columnModel.getColumn(column).setCellRenderer( this );
              // columnModel.getColumn(column).setCellEditor( this );
           }
         
         
         
           @Override
           public Component getTableCellRendererComponent(JTable table,
                   Object value, boolean isSelected, boolean hasFocus, int row, int column) {
               // TODO Auto-generated method stub

                   renderButton.addActionListener((ActionEvent e) -> {
                        System.out.println("Accion del Estudiante: "+row);
                    });
                // renderButton.setBackground(Color.WHITE);
                // renderButton.setBorderPainted(false);
                   renderButton.setIcon(new ImageIcon(getClass().getResource("/controlestudiantil/images/material_edit.png")));
                   return renderButton;
           }
         
    }
*/
}