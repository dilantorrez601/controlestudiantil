/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;

import java.util.ArrayList;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author David
 */
public class Conector {
    public static String BASE_DE_DATOS="vu1";
    public ArrayList<Estudiante> ListaDeEstudiantes()
    {
        ArrayList<Estudiante> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from estudiantes order by id_estudiante";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id_estudiante");
                String nombres = resultSet.getString("nombre");
                String apellidos = resultSet.getString("apellidos");
                int telefono = resultSet.getInt("telefono");
                //
                Estudiante obj_estudiante = new Estudiante();
                obj_estudiante.setId(id);
                obj_estudiante.setApellidos(apellidos);
                obj_estudiante.setNombres(nombres);
                obj_estudiante.setTelefono(telefono);
                obj_estudiante.setGenero((String) resultSet.getString("genero"));
                obj_estudiante.setNivel((String) resultSet.getString("nivel"));
                lista.add(obj_estudiante);

            }
            lista.forEach(x -> System.out.println(x.nombres));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public void GuardarEstudiante(Estudiante estudiante)
    {
       if(estudiante.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO estudiantes (nombre,apellidos,telefono,nivel,genero) " +
                            "VALUES ('"+estudiante.getNombres()+"', '"+estudiante.getApellidos()+"', "+estudiante.getTelefono()+",'"+estudiante.getNivel()+"','"+estudiante.getGenero()+"')");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE estudiantes  SET nombre='"+estudiante.getNombres()+"',apellidos='"+estudiante.getApellidos()+"',telefono="+estudiante.getTelefono()+",nivel='"+estudiante.getNivel()+"',genero='"+estudiante.getGenero()+"'   WHERE id_estudiante="+estudiante.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
    }
    
    public void EliminarEstudiante(Estudiante estudiante)
    {
        try { 

               try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                   Statement st = conn.createStatement();
                   st.executeUpdate("DELETE FROM estudiantes  WHERE id_estudiante="+estudiante.getId()+" ;");
               } 
           } catch (SQLException ex) {
               Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
           }
    }
    
    public ArrayList<Docente> ListaDocentes()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

               Docente docente= new Docente();
               
                docente.setId( (int) resultSet.getInt("id_docente") );
                docente.setNombre((String) resultSet.getString("nombre"));
                docente.setApellido((String) resultSet.getString("apellido"));
                docente.setSemestre((String) resultSet.getString("semestre"));
                docente.setCarrera((String) resultSet.getString("carrera"));
                docente.setRu((int) resultSet.getInt("ru"));
                docente.setTelefono((int) resultSet.getInt("telefono"));
                docente.setGenero((String) resultSet.getString("genero"));
                docente.setUniversidad((String) resultSet.getString("universidad"));
                lista.add(docente);

            }
            //lista.forEach(x -> System.out.println(x.nombres));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public ArrayList<Docente> ListaDeDocente()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id_docente");
                String nombre = resultSet.getString("nombre");
                String apellidos = resultSet.getString("apellido");
                String carrera = resultSet.getString("carrera");
                String semestre = resultSet.getString("semestre");
                int ru = resultSet.getInt("ru");
                int telefono = resultSet.getInt("telefono");
                String genero = resultSet.getString("genero");
                String universidad = resultSet.getString("universidad");
                //
                Docente obj_docente = new Docente();
                obj_docente.setId(id);
                obj_docente.setNombre(nombre);
                obj_docente.setApellido(apellidos);
                obj_docente.setCarrera(carrera);
                obj_docente.setSemestre(semestre);
                obj_docente.setRu(ru);                
                obj_docente.setTelefono(telefono);
                obj_docente.setGenero((String) resultSet.getString("genero"));
                obj_docente.setUniversidad((String) resultSet.getString("Universidad"));
                lista.add(obj_docente);

            }
            lista.forEach(x -> System.out.println(x.nombre));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public void GuardarDocente(Docente docente)
    {
       if(docente.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO docentes (nombre,apellido,carrera,semestre,ru,telefono,genero,universidad) " +
                            "VALUES ('"+docente.getNombre()+"', '"+docente.getApellido()+"', '"+docente.getCarrera()+"', '"+docente.getSemestre()+"', "+docente.getRu()+", "+docente.getTelefono()+",'"+docente.getGenero()+"','"+docente.getUniversidad()+"')");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE docentes  SET nombre='"+docente.getNombre()+"', apellido='"+docente.getApellido()+"', carrera='"+docente.getCarrera()+"', semestre='"+docente.getSemestre()+"', ru='"+docente.getRu()+"', telefono="+docente.getTelefono()+", genero='"+docente.getGenero()+"',universidad='"+docente.getUniversidad()+"'   WHERE id_docente="+docente.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
    }
    
    public void EliminarDocente(Docente docente)
    {
        try { 

               try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                   Statement st = conn.createStatement();
                   st.executeUpdate("DELETE FROM estudiantes  WHERE id_estudiante="+docente.getId()+" ;");
               } 
           } catch (SQLException ex) {
               Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
           }
    }
    
    public ArrayList<Docente> ListaDeDocentes()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

               Docente docente= new Docente();
               
                docente.setId( (int) resultSet.getInt("id_docente") );
                docente.setNombre((String) resultSet.getString("nombre"));
                docente.setApellido((String) resultSet.getString("apellido"));
                docente.setSemestre((String) resultSet.getString("semestre"));
                docente.setCarrera((String) resultSet.getString("carrera"));
                docente.setRu((int) resultSet.getInt("ru"));
                docente.setTelefono((int) resultSet.getInt("telefono"));
                docente.setGenero((String) resultSet.getString("genero"));
                docente.setUniversidad((String) resultSet.getString("universidad"));
                lista.add(docente);

            }
            //lista.forEach(x -> System.out.println(x.nombres));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
}
